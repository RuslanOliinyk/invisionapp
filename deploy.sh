#!/bin/bash

USER='root'
HOST='pensionplandecoder.com'
DEST='/var/www/pensionplandecoder.com'


mkdir dist

au build --env prod
mv scripts dist/
cp index.html dist/index.html

mkdir -p dist/src/assets
cp -R src/assets/ dist/src/assets

rsync -az --force --delete --progress --prune-empty-dirs -e "ssh -p22" ./dist/* $USER@$HOST:$DEST

rm -r dist