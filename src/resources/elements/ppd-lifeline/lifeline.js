import * as d3 from 'd3';

function lineChart(options) {
  const rawData = options.data;
  const chartID = options.chartID;
  const divWidth =
    options.width || document.getElementById(chartID).clientWidth;
  const divHeight =
    options.height || document.getElementById(chartID).clientHeight;

  const dispatch = d3.dispatch('ageChange');
  dispatch.on('ageChange', options.callback);

  const margin = { top: 20, right: 20, bottom: 80, left: 20 };
  const width = divWidth - margin.left - margin.right;
  const height = divHeight - margin.top - margin.bottom;

  const sliderCircleRadius = 16;
  const sliderHeight = 20;

  const arrowPath =
    'M377.941 169.941V216H134.059v-46.059c0-21.382-25.851-32.09-40.971-16.971L7.029 239.029c-9.373 9.373-9.373 24.568 0 33.941l86.059 86.059c15.119 15.119 40.971 4.411 40.971-16.971V296h243.882v46.059c0 21.382 25.851 32.09 40.971 16.971l86.059-86.059c9.373-9.373 9.373-24.568 0-33.941l-86.059-86.059c-15.119-15.12-40.971-4.412-40.971 16.97z';
  const arrowSize = 512;
  const arrowScale = 0.05;

  let data;
  let areaData;

  let selectedDatumIndex = 0;

  const ageScale = d3
    .scaleLinear()
    .range([0, width])
    .clamp(true);

  const monthlyScale = d3
    .scaleLinear()
    .range([height, height / 2])
    .nice();

  const capitalScale = d3
    .scaleLinear()
    .range([height, 0])
    .nice();

  const lineMonthly = d3
    .line()
    .x(d => ageScale(d.age))
    .y(d => monthlyScale(d.monthly));

  const areaMonthly = d3
    .area()
    .x(d => ageScale(d.age))
    .y0(d => {
      const y = monthlyScale(d.monthly);
      return y > height / 2 ? height : y + height / 2;
    })
    .y1(d => monthlyScale(d.monthly));

  const lineCapital = d3
    .line()
    .x(d => ageScale(d.age))
    .y(d => capitalScale(d.capital));

  const areaCapital = d3
    .area()
    .x(d => ageScale(d.age))
    .y0(d => {
      const y = capitalScale(d.capital);
      return y > height / 2 ? height : y + height / 2;
    })
    .y1(d => capitalScale(d.capital));

  const ageAxis = d3
    .axisBottom()
    .scale(ageScale)
    .tickSize(0)
    .tickPadding(sliderCircleRadius)
    .tickFormat(d3.format('d'));

  const monthlyAxis = d3
    .axisLeft()
    .scale(monthlyScale)
    .tickSize(0)
    .ticks(3)
    .tickFormat(d => {
      if (d === 0) {
        return '';
      } else {
        return `${d / 1000} Tsd.`;
      }
    })
    .tickPadding(0);

  const capitalAxis = d3
    .axisLeft()
    .scale(capitalScale)
    .tickSize(0)
    .ticks(5)
    .tickFormat(d => {
      if (capitalScale(d) > height / 2) {
        return '';
      } else {
        return `${d / 1000} Tsd.`;
      }
    })
    .tickPadding(0);

  const svg = d3
    .select(`#${chartID}`)
    .append('svg')
    .attr('width', divWidth)
    .attr('height', divHeight);

  const defs = svg.append('defs');
  const dropShadowID = generateDropShadow();
  const gradientID = generateGradient();

  const g = svg
    .append('g')
    .attr('transform', `translate(${margin.left},${margin.top})`);

  processData(rawData);
  render();

  function processData(originalData) {
    data = originalData.map(d => ({
      age: +d.age,
      monthly: +d.monthly,
      capital: +d.capital
    }));

    areaData = [];
    for (let i = 0; i < data.length - 1; i++) {
      areaData.push([data[i], data[i + 1]]);
    }

    const foundIndex = data.findIndex(d => d.age === 60);
    selectedDatumIndex = foundIndex === -1 ? 0 : foundIndex;
  }

  function render() {
    ageScale.domain(d3.extent(data, d => d.age));
    monthlyScale.domain([0, d3.max(data, d => d.monthly)]);
    capitalScale.domain([0, d3.max(data, d => d.capital)]);

    ageAxis.tickValues(data.map(d => +d.age));

    const areas = g.append('g').attr('class', 'areas');

    const area = areas
      .selectAll('.area')
      .data(areaData)
      .enter()
      .append('g')
      .attr('class', 'area')
      .style('display', (d, i) => (i >= selectedDatumIndex ? 'none' : 'block'));

    area
      .append('path')
      .attr('class', 'monthly-area')
      .attr('fill', `url(#${gradientID})`)
      .attr('d', areaMonthly);

    area
      .append('path')
      .attr('class', 'capital-area')
      .attr('fill', `url(#${gradientID})`)
      .attr('d', areaCapital);

    const lines = g
      .append('g')
      .attr('class', 'lines')
      .datum(data);

    lines
      .append('path')
      .attr('class', 'monthly-line')
      .attr('d', lineMonthly);

    lines
      .append('path')
      .attr('class', 'capital-line')
      .attr('d', lineCapital);

    const labels = g.append('g').attr('class', 'labels');

    labels
      .append('text')
      .attr('class', 'age-label')
      .attr('x', 10)
      .attr('y', height)
      .text('Pensionierungsalter');

    labels
      .append('text')
      .attr('class', 'monthly-label')
      .attr('x', 10)
      .attr('y', monthlyScale(data[selectedDatumIndex].monthly) - 25)
      .text('Monatliche Rente');

    labels
      .append('text')
      .attr('class', 'capital-label')
      .attr('x', 10)
      .attr('y', capitalScale(data[selectedDatumIndex].capital) - 25)
      .text('Altersguthaben');

    const slider = g
      .append('g')
      .attr('class', 'slider')
      .attr('transform', `translate(0,${height + sliderCircleRadius})`)
      .call(d3.drag().on('drag', dragged))
      .style('cursor', 'ew-resize');

    slider
      .append('rect')
      .attr('class', 'slider-track')
      .attr('x', -margin.left)
      .attr('y', -sliderHeight / 2)
      .attr('width', divWidth)
      .attr('height', sliderHeight);

    const axes = g.append('g').attr('class', 'axes');

    axes
      .append('g')
      .attr('class', 'axis age-axis')
      .attr('transform', `translate(0,${height})`)
      .call(ageAxis)
      .selectAll('.tick text')
      .attr('dy', '0.35em')
      .style('pointer-events', 'none');

    axes
      .append('g')
      .attr('class', 'axis monthly-axis')
      .attr('transform', `translate(${width},0)`)
      .call(monthlyAxis);

    axes
      .append('g')
      .attr('class', 'axis capital-axis')
      .attr('transform', `translate(${width},0)`)
      .call(capitalAxis);

    const focus = g
      .append('g')
      .attr('class', 'focus')
      .attr(
        'transform',
        `translate(${ageScale(data[selectedDatumIndex].age)},0)`
      )
      .style('pointer-events', 'none');

    focus
      .append('line')
      .attr('class', 'focus-line')
      .attr('x1', 0)
      .attr('y1', height)
      .attr('x2', 0)
      .attr('y2', capitalScale(data[selectedDatumIndex].capital));

    focus
      .append('circle')
      .attr('class', 'monthly focus-circle')
      .attr('cx', 0)
      .attr('cy', monthlyScale(data[selectedDatumIndex].monthly));

    focus
      .append('circle')
      .attr('class', 'capital focus-circle')
      .attr('cx', 0)
      .attr('cy', capitalScale(data[selectedDatumIndex].capital));

    focus
      .append('circle')
      .attr('class', 'slider-circle')
      .attr('cx', 0)
      .attr('cy', height + sliderCircleRadius)
      .attr('r', sliderCircleRadius)
      .attr('filter', `url(#${dropShadowID})`);

    focus
      .append('text')
      .attr('class', 'slider-circle-text')
      .attr('x', 0)
      .attr('y', height + sliderCircleRadius)
      .attr('dy', '0.35em')
      .style('text-anchor', 'middle')
      .text(data[selectedDatumIndex].age);

    focus
      .append('path')
      .attr('class', 'slider-arrow')
      .attr(
        'transform',
        `translate(${(-arrowSize * arrowScale) / 2},${height +
          sliderCircleRadius * 2})scale(${arrowScale})`
      )
      .attr('d', arrowPath);

    dispatch.call('ageChange', undefined, data[selectedDatumIndex]);

    function dragged() {
      const age = Math.round(ageScale.invert(d3.event.x));
      const index = data.findIndex(d => d.age === age);
      if (selectedDatumIndex === index) return;

      selectedDatumIndex = index;

      focus.attr(
        'transform',
        `translate(${ageScale(data[selectedDatumIndex].age)},0)`
      );

      focus
        .select('.focus-line')
        .attr('y2', capitalScale(data[selectedDatumIndex].capital));

      focus
        .select('.monthly.focus-circle')
        .attr('cy', monthlyScale(data[selectedDatumIndex].monthly));

      focus
        .select('.capital.focus-circle')
        .attr('cy', capitalScale(data[selectedDatumIndex].capital));

      focus.select('.slider-circle-text').text(age);

      area.style(
        'display',
        (d, i) => (i >= selectedDatumIndex ? 'none' : 'block')
      );

      dispatch.call('ageChange', undefined, data[selectedDatumIndex]);
    }
  }

  function generateDropShadow() {
    const dropShadowID = 'drop-shadow';
    const filter = defs
      .append('filter')
      .attr('id', dropShadowID)
      .attr('height', '130%');
    filter
      .append('feGaussianBlur')
      .attr('in', 'SourceAlpha')
      .attr('stdDeviation', 2)
      .attr('result', 'blur');
    filter
      .append('feOffset')
      .attr('in', 'blur')
      .attr('dx', 0)
      .attr('dy', 2)
      .attr('result', 'offsetBlur');
    filter
      .append('feComponentTransfer')
      .attr('in', 'offsetBlue')
      .attr('result', 'offsetBlurTransfer')
      .append('feFuncA')
      .attr('type', 'linear')
      .attr('slope', 0.2);
    const feMerge = filter.append('feMerge');
    feMerge.append('feMergeNode').attr('in', 'offsetBlurTransfer');
    feMerge.append('feMergeNode').attr('in', 'SourceGraphic');
    return dropShadowID;
  }

  function generateGradient() {
    const gradientID = 'gradient';
    const gradient = defs
      .append('linearGradient')
      .attr('id', gradientID)
      .attr('x1', '0%')
      .attr('y1', '0%')
      .attr('x2', '0%')
      .attr('y2', '100%');
    gradient
      .append('stop')
      .attr('offset', '0%')
      .attr('stop-color', '#e6eef4')
      .attr('stop-opacity', 1);
    gradient
      .append('stop')
      .attr('offset', '100%')
      .attr('stop-color', '#fff')
      .attr('stop-opacity', 0);
    return gradientID;
  }
}

export { lineChart };
