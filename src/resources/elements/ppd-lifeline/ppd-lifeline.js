import {
  inject,
  bindable,
  bindingMode,
  customElement
} from 'aurelia-framework';
import { EventAggregator } from 'aurelia-event-aggregator';

import { lineChart } from './lifeline';

@customElement('ppd-lifeline')
@inject(Element, EventAggregator)
export class PPDBar {
  @bindable({ defaultBindingMode: bindingMode.oneWay })
  data;

  constructor(element, eventAggregator) {
    this.element = element;
    this.ea = eventAggregator;

    this.elmId = this.generateId();

    this.demoData = [
      {
        age: '57',
        monthly: '1349',
        capital: '220000'
      },
      {
        age: '58',
        monthly: '2349',
        capital: '240000'
      },
      {
        age: '59',
        monthly: '3349',
        capital: '260000'
      },
      {
        age: '60',
        monthly: '4349',
        capital: '280000'
      },
      {
        age: '61',
        monthly: '5349',
        capital: '300000'
      },
      {
        age: '62',
        monthly: '6349',
        capital: '320000'
      },
      {
        age: '63',
        monthly: '7349',
        capital: '340000'
      },
      {
        age: '64',
        monthly: '8349',
        capital: '360000'
      },
      {
        age: '65',
        monthly: '9349',
        capital: '380000'
      },
      {
        age: '66',
        monthly: '10349',
        capital: '400000'
      },
      {
        age: '67',
        monthly: '11349',
        capital: '420000'
      },
      {
        age: '68',
        monthly: '12349',
        capital: '440000'
      },
      {
        age: '69',
        monthly: '13349',
        capital: '460000'
      },
      {
        age: '70',
        monthly: '14349',
        capital: '480000'
      }
    ];
  }

  get parsedData() {
    return this.data || this.demoData;
  }

  generateId() {
    let text = '';
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  attached() {
    const options = {
      data: this.parsedData,
      chartID: this.elmId,
      callback: data => {
        this.ea.publish('lifeline::select', data);
      },
      width: 705,
      height: 350
    };
    lineChart(options);
  }
}
