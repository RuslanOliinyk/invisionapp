import {
  inject,
  bindable,
  bindingMode,
  customElement,
  computedFrom
} from 'aurelia-framework';

import numeral from 'numeral';
import CountUp from 'countup';

@customElement('ppd-currency')
@inject(Element)
export class PpdCurrency {
  @bindable({ defaultBindingMode: bindingMode.oneWay })
  value;
  @bindable({ defaultBindingMode: bindingMode.oneWay })
  isMonth;
  @bindable({ defaultBindingMode: bindingMode.oneWay })
  animated;

  constructor(element) {
    this.element = element;
    this.elmId = Math.random();

    this.countUpOptions = {
      useEasing: true,
      useGrouping: true,
      separator: "'",
      decimal: '.',
      suffix: ' Fr.'
    };
  }

  @computedFrom('value')
  get formattedValue() {
    return `${numeral(this.value).format()}`;
  }

  valueChanged(newValue, oldValue) {
    if (!oldValue || oldValue < 1) {
      return;
    }

    this.animated = true;
    this.animate(oldValue, newValue, 0.5);
  }

  animate(from, to, duration) {
    new CountUp(
      this.elmId.toString(),
      from,
      to,
      0,
      duration || 2.5,
      this.countUpOptions
    ).start();
  }

  attached() {
    if (this.animated) {
      this.animate(0, this.value, 2.5);
    }
  }
}
