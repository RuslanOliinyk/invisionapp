import * as d3 from 'd3';

function donutChart(options) {
  const rawData = options.data;
  const chartID = options.chartID;
  const showLegend = options.showLegend;
  const transitionDuration = options.transitionDuration || 750;
  const divWidth =
    options.width || document.getElementById(chartID).clientWidth;
  const divHeight =
    options.height || document.getElementById(chartID).clientHeight;

  const margin = {
    top: 5,
    right: showLegend ? 350 : 5,
    bottom: 5,
    left: 5
  };
  const width = divWidth - margin.left - margin.right;
  const height = divHeight - margin.top - margin.bottom;

  const outerRadius = Math.min(width, height) / 2;
  const innerRadius = outerRadius - 15;

  const legendRectSize = outerRadius / 4;
  const legendItemPadding = outerRadius / 8;

  const format = d3.format('.0%');

  let data;

  const arc = d3
    .arc()
    .outerRadius(outerRadius)
    .innerRadius(innerRadius);

  const pie = d3
    .pie()
    .sort(null)
    .value(d => d.value);

  const svg = d3
    .select(`#${chartID}`)
    .append('svg')
    .attr('width', divWidth)
    .attr('height', divHeight);

  const g = svg
    .append('g')
    .attr(
      'transform',
      `translate(${margin.left + width / 2},${margin.top + height / 2})`
    );

  const legend = svg.append('g').attr('class', 'legend');

  processData(rawData);
  render();

  function processData(originalData) {
    data = originalData;
    data.forEach(d => {
      d.displayValue = d.value;
      d.value = +d.value.slice(0, d.value.length - 1);
    });
  }

  function render() {
    g.selectAll('.arc')
      .data(pie(data))
      .enter()
      .append('path')
      .attr('class', 'arc')
      .style('fill', d => d.data.color)
      .transition()
      .ease(d3.easeLinear)
      .delay((d, i) => (i * transitionDuration) / data.length)
      .duration(transitionDuration / data.length)
      .attrTween('d', function(d) {
        var i = d3.interpolate(d.startAngle, d.endAngle);
        return function(t) {
          d.endAngle = i(t);
          return arc(d);
        };
      });

    const valueLabel = data.filter(d => d.isSelected)[0];
    g.append('text')
      .attr('class', 'value-label')
      .style('text-anchor', 'middle')
      .attr('dy', '0.35em')
      .style('font-size', innerRadius / 3)
      .style('fill', valueLabel.color)
      .text('0')
      .transition()
      .duration(transitionDuration)
      .tween('text', function() {
        const that = d3.select(this);
        const i = d3.interpolateNumber(that.text(), valueLabel.value / 100);
        return function(t) {
          that.text(format(i(t)));
        };
      });

    if (!showLegend) return;

    const legendHeight =
      data.length * (legendRectSize + legendItemPadding) - legendItemPadding;

    legend.attr(
      'transform',
      `translate(${margin.left + width},${margin.top +
        height / 2 -
        legendHeight / 2})`
    );

    const legendItem = legend
      .selectAll('.legend-item')
      .data(data)
      .enter()
      .append('g')
      .attr('class', 'legend-item')
      .attr(
        'transform',
        (d, i) =>
          `translate(${legendRectSize}, ${i *
            (legendRectSize + legendItemPadding) +
            legendRectSize / 2})`
      );

    legendItem
      .append('rect')
      .attr('class', 'legend-rect')
      .attr('x', 0)
      .attr('y', -legendRectSize / 2)
      .attr('width', legendRectSize)
      .attr('height', legendRectSize)
      .attr('rx', legendRectSize / 6)
      .style('fill', d => d.color);

    legendItem
      .append('text')
      .attr('class', 'legend-label')
      .attr('x', legendRectSize * 1.75)
      .attr('dy', '0.35em')
      .style('font-size', legendRectSize / 1.5)
      .text(d => d.title);
  }
}

export { donutChart };
