import {
  inject,
  bindable,
  bindingMode,
  customElement
} from 'aurelia-framework';

import { donutChart } from './ppd-piechart';

@customElement('ppd-pie')
@inject(Element)
export class PPDPie {
  @bindable({ defaultBindingMode: bindingMode.oneWay })
  data;

  get parsedData() {
    return this.data || [];
  }

  constructor(element) {
    this.element = element;
    this.elmId = this.generateId();
  }

  generateId() {
    let text = '';
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  attached() {
    const options = {
      data: this.parsedData,
      chartID: this.elmId,
      transitionDuration: 1500,
      width: 200,
      height: 200,
      showLegend: false
    };
    donutChart(options);
  }
}
