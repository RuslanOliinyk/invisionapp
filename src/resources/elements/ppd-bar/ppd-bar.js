import {
  inject,
  bindable,
  bindingMode,
  customElement
} from 'aurelia-framework';

import { Chart } from 'chart.js';

@customElement('ppd-bar')
@inject(Element)
export class PPDBar {
  @bindable({ defaultBindingMode: bindingMode.oneWay })
  conf;

  get parsedConf() {
    return this.conf || {};
  }

  constructor(element) {
    this.element = element;
    this.elmId = this.generateId();
  }

  generateId() {
    let text = '';
    let possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
  }

  attached() {
    var ctx = document.getElementById(this.elmId).getContext('2d');
    new Chart(ctx, {
      type: 'bar',
      data: {
        labels: ['Red', 'Blue'],
        datasets: [
          {
            label: this.parsedConf.label,
            data: this.parsedConf.data,
            backgroundColor: this.parsedConf.colors || [
              'rgba(98, 164, 217, 1)',
              'rgba(225, 237, 247, 1)'
            ],
            borderWidth: 1
          }
        ]
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        legend: {
          display: false
        },
        tooltips: {
          enabled: false
        },
        scales: {
          yAxes: [
            {
              ticks: {
                beginAtZero: true,
                max: 100,
                min: 0,
                stepSize: 20,
                fontColor: '#757D88',
                fontStyle: 'bold',
                fontSize: 10,
                callback: function(value) {
                  return value + '%';
                }
              }
            }
          ],
          xAxes: [
            {
              ticks: {
                display: false
              },
              gridLines: {
                display: false
              }
            }
          ]
        }
      }
    });
  }
}
