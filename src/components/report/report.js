import { inject } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { EventAggregator } from 'aurelia-event-aggregator';

@inject(Router, EventAggregator)
export class Dashboard {
  constructor(router, eventAggregator) {
    this.router = router;
    this.ea = eventAggregator;

    // Pie Charts

    this.pieRisk = [
      {
        title: 'Risikobeitrag',
        color: '#60A5D9',
        value: '82%',
        isSelected: true
      },
      {
        title: 'Sparbeitrag',
        color: '#DEECF7',
        value: '18%',
        isSelected: false
      }
    ];
    this.pieTotal = [
      {
        title: 'Dein Arbeitgeber',
        color: '#60A5D9',
        value: '60%',
        isSelected: true
      },
      {
        title: 'Du Selbst',
        color: '#DEECF7',
        value: '40%',
        isSelected: false
      }
    ];

    // Bar Charts

    this.disabilityBar = {
      label: 'Einkommen',
      data: [48, 18]
    };

    this.deathBar = {
      label: 'Einkommen',
      data: [48, 18]
    };
  }

  attached() {
    this.subscription = this.ea.subscribe('lifeline::select', response => {
      this.selectedRetirementAge = response;
    });
  }

  detached() {
    this.subscription.dispose();
  }
}
