import { inject, observable } from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';
import { Router } from 'aurelia-router';

@inject(Router, HttpClient)
export class Dashboard {
  @observable files;

  constructor(router, http) {
    this.router = router;

    this.http = http;
    this.http.configure(config => {
      config.withBaseUrl('https://api.pensionplandecoder.com/api/v1/');
      //config.withBaseUrl('http://localhost:4005/api/v1/');
    });
  }

  filesChanged() {
    let file = this.files[0];

    let formData = new FormData();
    formData.append('file', file);

    this.http
      .fetch('upload', {
        method: 'POST',
        body: formData
      })
      .then(response => response.json())
      .then(ignore => {
        this.router.navigate('report');
        $('#uploadForm')
          .get(0)
          .reset();
      })
      .catch(error => {
        console.log(error);
        $('#uploadForm')
          .get(0)
          .reset();
      });
  }

  onUploadClick() {
    $('#uploadFile').trigger('click');
  }

  attached() {}
}
