import { inject } from 'aurelia-framework';
import { Router } from 'aurelia-router';
import { EventAggregator } from 'aurelia-event-aggregator';

@inject(Router, EventAggregator)
export class App {
  constructor(router, eventAggregator) {
    this.message = 'Hello World!';
    this.ea = eventAggregator;

    this.ea.subscribe('router:navigation:success', ignore => {
      $(window).scroll();
    });
  }

  configureRouter(config) {
    config.title = '';
    config.map([
      {
        route: ['', 'home'],
        moduleId: './components/home/home',
        title: 'Pensionplan Decoder'
      },
      {
        route: 'report',
        moduleId: './components/report/report',
        title: 'Grüezi Björn'
      }
    ]);
  }
}
