import environment from './environment';

import numeral from 'numeral';

export function configure(aurelia) {
  aurelia.use.standardConfiguration().feature('resources');

  if (environment.debug) {
    aurelia.use.developmentLogging();
  }

  if (environment.testing) {
    aurelia.use.plugin('aurelia-testing');
  }

  numeral.register('locale', 'de-ch', {
    delimiters: {
      thousands: "'",
      decimal: '.'
    },
    abbreviations: {
      thousand: 'k',
      million: 'm',
      billion: 'b',
      trillion: 't'
    },
    ordinal: function(number) {
      return '.';
    },
    currency: {
      symbol: 'CHF'
    }
  });
  numeral.locale('de-ch');

  aurelia
    .start()
    .then(() => aurelia.setRoot())
    .then(() => {
      runAnimation();
      ready();
    });
}

function runAnimation() {
  $.fn.scrollzip = function(options) {
    var settings = $.extend(
      {
        showFunction: null,
        hideFunction: null,
        showShift: 100,
        wholeVisible: false,
        hideShift: 0
      },
      options
    );

    return this.each(function(i, obj) {
      $(this).addClass('scrollzip');

      if ($.isFunction(settings.showFunction)) {
        if (
          !$(this).hasClass('isShown') &&
          $(window).outerHeight() +
            $('#scrollzipPoint').offset().top -
            settings.showShift >
            $(this).offset().top +
              (settings.wholeVisible ? $(this).outerHeight() : 0) &&
          $('#scrollzipPoint').offset().top +
            (settings.wholeVisible ? $(this).outerHeight() : 0) <
            $(this).outerHeight() + $(this).offset().top - settings.showShift
        ) {
          $(this).addClass('isShown');
          settings.showFunction.call(this);
        }
      }

      if ($.isFunction(settings.hideFunction)) {
        if (
          $(this).hasClass('isShown') &&
          ($(window).outerHeight() +
            $('#scrollzipPoint').offset().top -
            settings.hideShift <
            $(this).offset().top +
              (settings.wholeVisible ? $(this).outerHeight() : 0) ||
            $('#scrollzipPoint').offset().top +
              (settings.wholeVisible ? $(this).outerHeight() : 0) >
              $(this).outerHeight() + $(this).offset().top - settings.hideShift)
        ) {
          $(this).removeClass('isShown');
          settings.hideFunction.call(this);
        }
      }

      return this;
    });
  };

  setTimeout(function() {
    startAnimation();
  }, 200);

  $(window).on('scroll resize', function() {
    startAnimation();
  });
}

function ready() {
  // Collapse
  $(document).on('click', '.custom_collapse .title_collapse', function() {
    $(this).toggleClass('open_arrow');
    $(this)
      .closest('.custom_collapse')
      .find('.content_collapse')
      .slideToggle('slow');

    $('.close_btn').click(function() {
      $(this)
        .closest('.content_collapse')
        .slideUp('slow');
    });
  });

  // Scroll top
  $(document).on('click', '.scroll_top', function() {
    $('html, body').animate({ scrollTop: 0 }, 800);
    return false;
  });

  // popup
  $('.modal-toggle').click(function(){
      $(this).find('.modal').toggleClass('is-visible');
  });

}

function startAnimation() {
  $('.jsbounceInLeft').scrollzip({
    showFunction: function() {
      $(this)
        .css('visibility', 'visible')
        .addClass('bounceInLeft');
    }
  });

  $('.jsbounceInRight').scrollzip({
    showFunction: function() {
      $(this)
        .css('visibility', 'visible')
        .addClass('bounceInRight');
    }
  });

  $('.jsbounceInDown').scrollzip({
    showFunction: function() {
      $(this)
        .css('visibility', 'visible')
        .addClass('bounceInDown');
    }
  });

  $('.jsbounceInUp').scrollzip({
    showFunction: function() {
      $(this)
        .css('visibility', 'visible')
        .addClass('bounceInUp');
    }
  });

  $('.jsbounce').scrollzip({
    showFunction: function() {
      $(this)
        .css('visibility', 'visible')
        .addClass('bounce');
    }
  });

  $('.jsfadeInUp').scrollzip({
    showFunction: function() {
      $(this)
        .css('visibility', 'visible')
        .addClass('fadeInUp');
    }
  });

  $('.jsfadeInDown').scrollzip({
    showFunction: function() {
      $(this)
        .css('visibility', 'visible')
        .addClass('fadeInDown');
    }
  });

  $('.jsfadeInLeft').scrollzip({
    showFunction: function() {
      $(this)
        .css('visibility', 'visible')
        .addClass('fadeInLeft');
    }
  });

  $('.jsfadeInRight').scrollzip({
    showFunction: function() {
      $(this)
        .css('visibility', 'visible')
        .addClass('fadeInRight');
    }
  });

  $('.jsfadeIn').scrollzip({
    showFunction: function() {
      $(this)
        .css('visibility', 'visible')
        .addClass('fadeIn');
    }
  });

  $('.jszoomIn').scrollzip({
    showFunction: function() {
      $(this)
        .css('visibility', 'visible')
        .addClass('zoomIn');
    }
  });
}
